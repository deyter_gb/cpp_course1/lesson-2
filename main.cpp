#include <iostream>
#include <sstream>

const int LONG_ARRAY_SIZE = 9;
const int SMALL_ARRAY_SIZE = 3;

union MyData {
    int i_var;
    float f_var;
    char c_var;
};

struct MyVariant {
  MyData md_data;
  unsigned short isInt: 1;
  unsigned short isFloat: 1;
  unsigned short isChar: 1;
};

/* #region Helper functions */
/**
 * Convert MyVariant structure to string.
 *
 * @param MyVariant mv_var
 * Structure to convert.
 * @return
 * Value from structure converted to string.
 */
std::string MyVariantToString(MyVariant *mv_var) {
  if (mv_var->isInt) {
    std::ostringstream ss;
    ss << mv_var->md_data.i_var;
    std::string s(ss.str());
    return s;
  } else if (mv_var->isChar) {
    std::ostringstream ss;
    ss << mv_var->md_data.c_var;
    std::string s(ss.str());
    return s;
  } else if (mv_var->isFloat) {
    std::ostringstream ss;
    ss << mv_var->md_data.f_var;
    std::string s(ss.str());
    return s;
  }
  throw std::invalid_argument("Flags is set incorrect or unitialized structure.");
}

/**
 * Set Int value, clear all other values and set correct flags in structure.
 *
 * @param mv_var MyVariant *
 * Structure to change
 * @param i_value int
 * Value.
 */
void SetValue(MyVariant *mv_var, int i_value) {
  mv_var->md_data.i_var = i_value;
  mv_var->isInt = 1;
  mv_var->isFloat = 0;
  mv_var->isChar = 0;
}

/**
 * Set Float value, clear all other values and set correct flags in structure.
 *
 * @param mv_var MyVariant *
 * Structure to change
 * @param f_value float
 * Value.
 */
void SetValue(MyVariant *mv_var, float f_value) {
  mv_var->md_data.f_var = f_value;
  mv_var->isInt = 0;
  mv_var->isFloat = 1;
  mv_var->isChar = 0;
}

/**
 * Set Char value, clear all other values and set correct flags in structure.
 *
 * @param mv_var MyVariant *
 * Structure to change
 * @param c_value char
 * Value.
 */
void SetValue(MyVariant *mv_var, char c_value) {
  mv_var->md_data.c_var = c_value;
  mv_var->isInt = 0;
  mv_var->isFloat = 0;
  mv_var->isChar = 1;
}
/* #endregion */

int main() {
  // Task 1.
  /**
   * Создать и инициализировать переменные пройденных типов данных (short
   * int, int, long long, char, bool, float, double).
   */
  {
    short int si_var = 12;
    int i_var = 5'247;
    long long ll_var = 1'234'567'890'123'456'789;
    char c_var = 'c';
    bool b_var = false;
    float f_var = 1.057f;
    double d_var = 1.25E-3;
  }

  {
    // Task 2.
    /**
     * Создать перечисление (enum) с возможными вариантами символов для
     * игры в крестики-нолики.
     */
    enum EXOGameFieldsValues { X, O, empty };

    // Task 3.
    /**
     * Создать массив, способный содержать значения такого перечисления и
     * инициализировать его.
     */
    EXOGameFieldsValues exgfv_arr[LONG_ARRAY_SIZE] = {empty, empty, empty, empty, empty, empty, empty, empty, empty};

    // Task 4.
    /**
     * Создать структуру (struct) данных «Поле для игры в крестики-нолики» и
     * снабдить его всеми необходимыми свойствами (подумайте что может
     * понадобиться).
     */
    struct BoardXOGame {
      /**
       * В поле для игры в XO не нужны никакие другие свойства кроме самих полей, а для контроля хода игры нужен
       * другой объект, и в нём уже будет содержаться поле, номер хода, победитель итд...
       */
      EXOGameFieldsValues exgfv_line1[SMALL_ARRAY_SIZE] = {empty, empty, empty};
      EXOGameFieldsValues exgfv_line2[SMALL_ARRAY_SIZE] = {empty, empty, empty};
      EXOGameFieldsValues exgfv_line3[SMALL_ARRAY_SIZE] = {empty, empty, empty};
    };

    // Task 5.
    /**
     * Создать структуру (struct MyVariant) объединяющую: union MyData (int,
     * float, char) и 3-и битовых поля (флага) указывающими какого типа значение в
     * данный момент содержится в объединении (isInt, isFloat, isChar).
     * Продемонстрировать пример использования в коде этой структуры.
     */
    {
      // Declared at the top.
//      union MyData {
//          int i_var;
//          float f_var;
//          char c_var;
//      };
//
//      struct MyVariant {
//          MyData md_data;
//          unsigned short isInt : 1;
//          unsigned short isFloat : 1;
//          unsigned short isChar : 1;
//      };

      // Example of usage.
      auto mv_var = new MyVariant{ {11}, 1, 0, 0 };

      SetValue(mv_var, 1.7f);
      std::cout << MyVariantToString(mv_var) << std::endl;
      SetValue(mv_var, 'Z');
      std::cout << MyVariantToString(mv_var) << std::endl;
      SetValue(mv_var, 42);
      std::cout << MyVariantToString(mv_var) << std::endl;
    }
  }

  return 0;
}
